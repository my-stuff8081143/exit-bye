#include <ncurses.h>
#include <locale.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
   setlocale(LC_ALL, "");
   initscr();
   curs_set(0);

   if (argc != 1) {
      if (strcmp(argv[1], "--red") == 0) {
         start_color();
         init_pair(1, COLOR_RED, COLOR_BLACK);
         attrset(COLOR_PAIR(1));
      }

      else if (strcmp(argv[1], "--green") == 0) {
         start_color();
         init_pair(1, COLOR_GREEN, COLOR_BLACK);
         attrset(COLOR_PAIR(1));
      }

      else if (strcmp(argv[1], "--yellow") == 0) {
         start_color();
         init_pair(1, COLOR_YELLOW, COLOR_BLACK);
         attrset(COLOR_PAIR(1));
      }

      else if (strcmp(argv[1], "--blue") == 0) {
         start_color();
         init_pair(1, COLOR_BLUE, COLOR_BLACK);
         attrset(COLOR_PAIR(1));
      }

      else if (strcmp(argv[1], "--magenta") == 0) {
         start_color();
         init_pair(1, COLOR_MAGENTA, COLOR_BLACK);
         attrset(COLOR_PAIR(1));
      }

      else if (strcmp(argv[1], "--cyan") == 0) {
         start_color();
         init_pair(1, COLOR_CYAN, COLOR_BLACK);
         attrset(COLOR_PAIR(1));
      }

      else if (strcmp(argv[1], "--help") == 0) {
         clear();

         move((LINES /2) -4, (COLS /2) -6);
         printw("LIST OF ARGS");
         move((LINES /2) -2, (COLS /2) -13);
         printw("--red: Sets FG Color to red");
         move((LINES /2) -1, (COLS /2) -13);
         printw("--green: Sets FG Color to green");
         move((LINES /2), (COLS /2) -13);
         printw("--yellow: Sets FG Color to yellow");
         move((LINES /2) +1, (COLS /2) -13);
         printw("--blue: Sets FG Color to blue");
         move((LINES /2) +2, (COLS /2) -13);
         printw("--magenta: Sets FG Color to magenta");
         move((LINES /2) +3, (COLS /2) -13);
         printw("--cyan: Sets FG Color to cyan");
         move((LINES /2) +4, (COLS /2) -13);
         printw("--help: Prints this list :)");

         getch();
         endwin();
         return 0;
      }
   }

   clear();

   move((LINES /2) -2, (COLS /2) -16);
   printw("██████  ██    ██ ███████     ██");
   move((LINES /2) -1, (COLS /2) -16);
   printw("██   ██  ██  ██  ██          ██");
   move((LINES /2), (COLS /2) -16);
   printw("██████    ████   █████       ██");
   move((LINES /2) +1, (COLS /2) -16);
   printw("██   ██    ██    ██            ");
   move((LINES /2) +2, (COLS /2) -16);
   printw("██████     ██    ███████     ██");

   refresh();

   system("sleep 1s");

   endwin();
   return 0;
}
